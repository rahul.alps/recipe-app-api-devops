terraform {
  backend "s3" {
    bucket         = "elasticbeanstalk-us-east-1-753699145791"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environmet = terraform.workspace
    project    = var.project
    Owner      = var.contact
    ManagedBy  = "Terraform"
  }
}


